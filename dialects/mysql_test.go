package dialects

import (
	"context"
	"database/sql"
	"reflect"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"xorm.io/xorm/core"
	"xorm.io/xorm/schemas"
)

// TestParseQuotedOptions tests the parseQuotedOptions function

// DBQueryer wraps a *sql.DB to implement core.Queryer
type DBQueryer struct {
	*sql.DB
}

// QueryContext implements core.Queryer
func (db *DBQueryer) QueryContext(ctx context.Context, query string, args ...interface{}) (*core.Rows, error) {
	rows, err := db.DB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	return &core.Rows{Rows: rows}, nil
}

// TestGetColumns tests the GetColumns function with a real MySQL database
func TestGetColumns(t *testing.T) {
	// Skip this test if we're running in a CI environment or don't want to connect to a real DB
	// t.Skip("Skipping test that requires a real MySQL database")

	// Connect to the MySQL database
	db, err := sql.Open("mysql", "root:123456@tcp(127.0.0.1:3306)/sakila")
	if err != nil {
		t.Fatalf("Failed to connect to MySQL: %v", err)
	}
	defer db.Close()

	// Ping the database to verify the connection
	if err := db.Ping(); err != nil {
		t.Fatalf("Failed to ping MySQL: %v", err)
	}

	// Create a queryer from the database connection
	queryer := &DBQueryer{DB: db}

	// Create MySQL dialect
	dialect := QueryDialect("mysql")
	uri := &URI{DBName: "sakila"}
	dialect.Init(uri)

	// Call GetColumns for the film table
	colSeq, cols, err := dialect.GetColumns(queryer, context.Background(), "film")
	if err != nil {
		t.Fatalf("GetColumns failed: %v", err)
	}

	// Verify we got the expected number of columns
	if len(colSeq) == 0 {
		t.Errorf("Expected columns, got none")
	}

	// Test ENUM column (rating)
	ratingCol, ok := cols["rating"]
	if !ok {
		t.Fatalf("rating column not found")
	}
	if ratingCol.SQLType.Name != schemas.Enum {
		t.Errorf("Expected rating column type to be %s, got %s", schemas.Enum, ratingCol.SQLType.Name)
	}

	// Verify the enum options
	expectedEnumOptions := map[string]int{
		"G":     0,
		"PG":    1,
		"PG-13": 2,
		"R":     3,
		"NC-17": 4,
	}
	if !reflect.DeepEqual(ratingCol.EnumOptions, expectedEnumOptions) {
		t.Errorf("Expected enum options %v, got %v", expectedEnumOptions, ratingCol.EnumOptions)
	}

	// Test SET column (special_features)
	featuresCol, ok := cols["special_features"]
	if !ok {
		t.Fatalf("special_features column not found")
	}

	// Print debug information
	t.Logf("special_features column type: %s", featuresCol.SQLType.Name)
	t.Logf("special_features options: %v", featuresCol.SetOptions)

	if featuresCol.SQLType.Name != schemas.Set {
		t.Errorf("Expected special_features column type to be %s, got %s", schemas.Set, featuresCol.SQLType.Name)
	}

	// Verify the set options
	expectedSetOptions := map[string]int{
		"Trailers":          0,
		"Commentaries":      1,
		"Deleted Scenes":    2,
		"Behind the Scenes": 3,
	}
	if !reflect.DeepEqual(featuresCol.SetOptions, expectedSetOptions) {
		t.Errorf("Expected set options %v, got %v", expectedSetOptions, featuresCol.SetOptions)
	}
}
